module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./hooks/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        "nord-polar-darkest": "#2e3440",
        "nord-polar-dark": "#3b4252",
        "nord-polar-light": "#434c5e",
        "nord-polar-lightest": "#4c566a",
        "nord-snow-dark": "#d8dee9",
        "nord-snow-light": "#eceff4",
        "nord-aurora-red": "#bf616a",
        "nord-aurora-orange": "#d08770",
        "nord-aurora-yellow": "#ebcb8b",
        "nord-aurora-green": "#a3be8c",
        "nord-aurora-violet": "#b48ead",
        "nord-frost-blue": "#5e81ac",
      },
    },
  },
  plugins: [],
};
