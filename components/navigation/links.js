import {
  CalendarIcon,
  ChartBarIcon,
  FolderIcon,
  HomeIcon,
  InboxIcon,
  MenuIcon,
  UsersIcon,
  XIcon,
  UserIcon,
} from "@heroicons/react/outline";
import { FaHSquare } from "react-icons/fa";
import { BiMovie } from "react-icons/bi";


export const navigation = [
  { name: "Home", href: "/", icon: HomeIcon },
  { name: "Resume", href: "/resume", icon: UserIcon },
  { name: "Wordle", href: "/wordle", icon: FaHSquare },
  { name: "Movies", href: "/movies", icon: BiMovie },
];
