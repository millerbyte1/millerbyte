import React, { useEffect } from "react";
import classNames from "classnames";
import { navigation } from "./links";
import Link from "next/link";
import { useUser } from "@auth0/nextjs-auth0";
import { useRouter } from "next/router";

const Sidebar = ({ fullscreen }) => {
  const { user, error, isLoading } = useUser();
  const router = useRouter();

  useEffect(() => {
    console.log(router);
  }, []);

  return !fullscreen ? (
    <div className="hidden md:fixed md:inset-y-0 md:flex md:w-64 md:flex-col">
      {/* Sidebar component, swap this element with another sidebar if you like */}
      <div className="flex min-h-0 flex-1 flex-col bg-gray-800">
        <div className="flex flex-1 flex-col overflow-y-auto pt-5 pb-4">
          <div className="flex flex-shrink-0 items-center px-4">
            <img
              className="h-8 w-auto"
              src="https://tailwindui.com/img/logos/workflow-logo-indigo-500-mark-white-text.svg"
              alt="Workflow"
            />
          </div>
          <nav className="mt-5 flex-1 space-y-1 px-2">
            {navigation.map((item) => (
              <Link key={item.name} href={item.href}>
                <div
                  className={classNames(
                    router.pathname == item.href
                      ? "bg-gray-900 text-white"
                      : "text-gray-300 hover:bg-gray-700 hover:text-white",
                    "group flex cursor-pointer items-center rounded-md px-2 py-2 text-sm font-medium"
                  )}
                >
                  <item.icon
                    className={classNames(
                      router.pathname == item.href
                        ? "text-gray-300"
                        : "text-gray-400 group-hover:text-gray-300",
                      "mr-3 h-6 w-6 flex-shrink-0"
                    )}
                    aria-hidden="true"
                  />
                  {item.name}
                </div>
              </Link>
            ))}
          </nav>
        </div>
        <div className="flex flex-shrink-0 justify-center bg-gray-700 p-4">
          {user && (
            <Link href="/profile">
              <div className="group block w-full flex-shrink-0 cursor-pointer">
                <div className="flex items-center">
                  <div>
                    <img
                      className="inline-block h-9 w-9 rounded-full"
                      src={user.picture}
                      alt=""
                    />
                  </div>
                  <div className="ml-3">
                    <p className="text-sm font-medium text-white">
                      {user.name}
                    </p>
                    <p className="text-xs font-medium text-gray-300 group-hover:text-gray-200">
                      View profile
                    </p>
                  </div>
                </div>
              </div>
            </Link>
          )}
          {!user && (
            <Link href="/api/auth/login">
              <p className="cursor-pointer font-medium text-gray-300 group-hover:text-gray-200">
                Login
              </p>
            </Link>
          )}
        </div>
      </div>
    </div>
  ) : null;
};

export default Sidebar;
