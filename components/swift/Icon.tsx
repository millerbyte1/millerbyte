import { DocumentIcon } from "@heroicons/react/outline";
import React from "react";

export default function Icon() {
  return (
    <div className=" h-20 w-20 shrink-0 cursor-pointer rounded-lg border-4 border-gray-300 bg-gray-200  hover:opacity-60">
      <div className="flex flex-col items-center justify-center">
        <DocumentIcon className=" h-12 w-12 text-gray-600"></DocumentIcon>
        <div className="font-bold text-gray-600">Swift 1</div>
      </div>
    </div>
  );
}
