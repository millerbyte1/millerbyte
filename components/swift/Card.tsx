import React from "react";

export default function Card() {
  return (
    <div className=" h-28 w-28 cursor-pointer rounded-xl border-4 border-gray-300 bg-gray-200  hover:opacity-60"></div>
  );
}
