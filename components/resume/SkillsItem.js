import React, { Fragment } from "react";

const skillLevel = {
  1: "ok",
  2: "Proficient",
  3: "Skilled",
  4: "Great",
  5: "Excellent",
};

export default function SkillsItem({ skill, level }) {
  var skillIcons = Array.apply(null, Array(5)).map((a, i) => {
    return i < level ? (
      <div className="mr-2 h-4 w-4 rounded-full bg-violet-500" key={i}></div>
    ) : (
      <div className="mr-2 h-4 w-4 rounded-full bg-gray-200" key={i}></div>
    );
  });

  var skillText = skillLevel[level];

  return (
    <div className=" flex flex-col items-center justify-center">
      <div className="mb-1 text-center text-xl font-bold text-gray-700">
        {skill}
      </div>
      <div className="flex">{skillIcons}</div>
      <div className=" text-sm italic">{skillText}</div>
    </div>
  );
}
