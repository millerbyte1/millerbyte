import React from "react";

const Header = ({ text }) => {
  return (
    <div className="absolute flex h-12 w-full items-center justify-center rounded-t-lg  bg-gradient-to-tl from-violet-800 to-violet-400 text-2xl font-bold text-white">
      {text}
    </div>
  );
};

export default Header;
