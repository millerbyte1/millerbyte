import { ChevronRightIcon } from "@heroicons/react/solid";
import React from "react";

const ExperienceDescriptionItem = ({ text, style = "mb-2" }) => {
  return (
    <div className={`${style} flex items-center`}>
      <ChevronRightIcon className="mt-1 h-6 w-6 shrink-0 text-violet-700"></ChevronRightIcon>{" "}
      {text}
    </div>
  );
};

export default ExperienceDescriptionItem;
