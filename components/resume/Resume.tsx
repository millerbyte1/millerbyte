import React, { Fragment } from "react";
import { ChevronRightIcon, ArrowLeftIcon } from "@heroicons/react/solid";
import { MailIcon, PhoneIcon } from "@heroicons/react/outline";
import SkillsItem from "./SkillsItem";
import { FaLinkedin, FaGithubSquare, FaCodepen } from "react-icons/fa";
import ExperiencedescriptionItem from "./ExperienceDescriptionItem";
import ExperiencePosition from "./ExperiencePosition";
import Header from "./Header";
import Link from "next/link";
import Image from "next/image";

export default function ResumeComponent() {
  return (
    <div className=" relative grid grid-rows-6 gap-2 p-4 md:gap-4">
      <div className="row-span-4 grid grid-cols-12 gap-2 md:gap-4">
        <div className=" relative col-span-12 rounded-lg bg-white shadow-md 2xl:col-span-3">
          <Header text="Profile"></Header>
          <div className="grid h-full grid-rows-6 pt-12">
            <div className="  row-span-3 flex flex-wrap items-center justify-center bg-white px-4 pt-4">
              <div className="h-48 w-48 shrink-0 rounded-full">
                <div className="absolute h-48 w-48 shrink-0 rounded-full bg-purple-800 antialiased opacity-60 mix-blend-color"></div>
                <img
                  className=" box-content h-48 w-48 shrink-0 rounded-full border-4 mix-blend-normal shadow-lg brightness-125 contrast-125 grayscale"
                  src={"/profile_cropped_more.jpg"}
                ></img>
              </div>

              <div className="flex flex-col items-center xl:ml-3">
                <div className=" text-center text-3xl font-bold text-violet-600">
                  Matthew Anderson
                </div>
                <div className=" text-xl italic text-slate-600">
                  Software Engineer
                </div>
                <div className="mt-3">B.S. Software Engineering</div>
                <div className="">B.A. Minor, Art & Design</div>
              </div>
            </div>
            <div className=" row-span-3 flex items-center px-6">
              <div className="text-2xl text-slate-600">
                I love unpacking complex problems and developing creative
                solutions that feel natural, look good, and adapt gracefully.
                Game Developer, Digital / 3D Artist, chess enthusiast, budding
                Youtube Educator, hobby collector.
              </div>
            </div>
          </div>
        </div>
        <div className=" relative col-span-12 rounded-lg bg-white shadow-md 2xl:col-span-9">
          <Header text="Experience"></Header>
          <div className="grid h-full grid-cols-12 grid-rows-6 pt-12">
            <div className="col-span-12 row-span-2 grid grid-cols-12 xl:col-span-6 xl:row-span-3 ">
              <div className="col-span-full sm:col-span-4">
                <ExperiencePosition
                  title="Senior DevSecOps Engineer"
                  location="Lockheed Martin"
                  dates="May 2021 - Present"
                  className=" bg-violet-50"
                ></ExperiencePosition>
              </div>
              <div className="col-span-full sm:col-span-8">
                <div className="flex h-full flex-col justify-center bg-violet-50 p-4">
                  <ExperiencedescriptionItem
                    text="Develop and Maintain Software Factory tool for deploying and orchestrating
      fully integrated software environments using Python, Ansible, Terraform,
      Helm"
                  ></ExperiencedescriptionItem>
                  <ExperiencedescriptionItem
                    text="Create strategies for testing, merging, and publishing
                  releases for project code within a CI/CD Pipeline"
                  ></ExperiencedescriptionItem>
                  <ExperiencedescriptionItem
                    text="Automatically generate and publish user-friendly project
                  documentation within a CI/CD Pipeline"
                  ></ExperiencedescriptionItem>
                </div>
              </div>
            </div>

            <div className="col-span-12 row-span-2 grid grid-cols-12 xl:col-span-6 xl:row-span-3">
              <div className="col-span-full sm:col-span-4">
                <ExperiencePosition
                  title="Full Stack Engineer"
                  location="CHSI Technologies"
                  dates="Aug 2020 - May 2021"
                  className=""
                ></ExperiencePosition>
              </div>
              <div className="col-span-full sm:col-span-8">
                <div className="flex h-full flex-col justify-center p-4">
                  <ExperiencedescriptionItem
                    text="Migrate monolith .NET application to cloud-based microservice
                  architecture using Microsoft Azure"
                  ></ExperiencedescriptionItem>
                  <ExperiencedescriptionItem text="Create and configure CI/CD pipelines with Azure DevOps"></ExperiencedescriptionItem>
                  <ExperiencedescriptionItem text="Unit, Integration, and Regression Testing"></ExperiencedescriptionItem>
                  <ExperiencedescriptionItem text="Add quality of life features, including:"></ExperiencedescriptionItem>
                  <div className=" ml-8 flex flex-col">
                    <ExperiencedescriptionItem text="Database insights dashboard with performance metrics"></ExperiencedescriptionItem>
                    <ExperiencedescriptionItem text="Automated Azure Alerts and Functions"></ExperiencedescriptionItem>
                    <ExperiencedescriptionItem text="Report Logging"></ExperiencedescriptionItem>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-span-12 row-span-2 grid grid-cols-12 xl:col-span-6 xl:row-span-3 ">
              <div className="col-span-full sm:col-span-4">
                <ExperiencePosition
                  title="Full Stack Engineer"
                  location="Agilutions"
                  dates="Apr 2018 - Aug 2020"
                  className=" rounded-bl-lg bg-violet-50 xl:bg-white"
                ></ExperiencePosition>
              </div>
              <div className="col-span-full sm:col-span-8">
                <div className="flex h-full flex-col justify-center bg-violet-50 p-4 text-sm xl:bg-white">
                  <ExperiencedescriptionItem
                    style="mb-1"
                    text="Perform custom implementations of an Enterprise AMS with
                  emphasis on credentialing using ASP.NET and Microsoft SQL
                  Server"
                  ></ExperiencedescriptionItem>
                  <ExperiencedescriptionItem
                    style="mb-1"
                    text="Work directly with clients during every phase of a project,
                  from requirements gathering, database schema design, RESTful
                  API development and consumption, front-end reskinning, UAT and
                  delivery"
                  ></ExperiencedescriptionItem>
                  <ExperiencedescriptionItem
                    text="Design and Develop commercial software including:"
                    style="mb-1"
                  ></ExperiencedescriptionItem>
                  <div className=" ml-8 flex flex-col">
                    <ExperiencedescriptionItem
                      text="Customizable Online Marketplace"
                      style="mb-1"
                    ></ExperiencedescriptionItem>
                    <ExperiencedescriptionItem
                      text="Multi-step Installment Membership On-boarding process"
                      style="mb-1"
                    ></ExperiencedescriptionItem>
                    <ExperiencedescriptionItem
                      text="Coupon Management and Distribution System"
                      style="mb-1"
                    ></ExperiencedescriptionItem>
                  </div>
                  <ExperiencedescriptionItem
                    text="Research and Development on standalone Credentialing
                  Management Software"
                    style="mb-1"
                  ></ExperiencedescriptionItem>
                  <ExperiencedescriptionItem
                    text="Strong database fundamentals with complex SQL Stored
                  Procedures, recursive CTEs, and JSON / XML pathing"
                    style="mb-1"
                  ></ExperiencedescriptionItem>
                  <ExperiencedescriptionItem
                    text="Create custom ASP.NET Web Forms controls and Web Services"
                    style="mb-1"
                  ></ExperiencedescriptionItem>
                  <ExperiencedescriptionItem
                    text="Deploy schema and metadata changes to Development, Test, and
                  Production environments"
                    style="mb-1"
                  ></ExperiencedescriptionItem>
                </div>
              </div>
            </div>

            <div className="col-span-12 row-span-2 grid grid-cols-12 xl:col-span-6 xl:row-span-3 ">
              <div className="col-span-full sm:col-span-4">
                <ExperiencePosition
                  title="Software Developer"
                  location="Encompass Technologies"
                  dates="Dec 2016 - Nov 2017"
                  className=" bg-white xl:bg-violet-50 "
                ></ExperiencePosition>
              </div>
              <div className="col-span-full sm:col-span-8">
                <div className="flex h-full flex-col justify-around rounded-br-lg bg-white p-4 xl:bg-violet-50">
                  <ExperiencedescriptionItem
                    text="Resolve and reduce customer-facing bugs on the Encompass UI
                  Team"
                  ></ExperiencedescriptionItem>
                  <ExperiencedescriptionItem
                    text="Design and maintain web interfaces to relay important customer
                  relations events and internal events with clarity"
                  ></ExperiencedescriptionItem>
                  <ExperiencedescriptionItem
                    text="Consult with internal customers and deliver software that
                  addresses and simplifies Human Resource related challenges"
                  ></ExperiencedescriptionItem>
                  <ExperiencedescriptionItem
                    text="Develop interactive guides to encourage successful customer
                  onboarding and minimize software-related support tickets"
                  ></ExperiencedescriptionItem>
                  <ExperiencedescriptionItem
                    text="Coordinate with Product Managers utilizing SCRUM methods to
                  ensure interactive guides are delivered promptly and
                  completely, while adhering to industry standards and best
                  practices"
                  ></ExperiencedescriptionItem>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row-span-2 grid grid-cols-12 gap-2 md:gap-4">
        <div className=" relative col-span-12 rounded-lg bg-white shadow-md xl:col-span-8">
          <Header text="Skills"></Header>

          <div className="grid h-full grid-cols-2 pt-12">
            <div className="col-span-full flex flex-col md:col-span-1">
              <div className="flex h-12 items-center justify-center bg-violet-50 text-xl font-bold text-slate-800">
                Tools
              </div>
              <div className="bg-violet-5 grid grow grid-cols-2  gap-2 bg-violet-50 sm:grid-cols-3 md:gap-4 md:rounded-bl-lg">
                <SkillsItem
                  skill="C# / .NET"
                  key="csharp"
                  level={5}
                ></SkillsItem>
                <SkillsItem
                  skill="NodeJS / Express"
                  key="node"
                  level={4}
                ></SkillsItem>
                <SkillsItem skill="AWS" key="aws" level={3}></SkillsItem>
                <SkillsItem skill="SQL" key="sql" level={5}></SkillsItem>
                <SkillsItem skill="CI/CD" key="cicd" level={5}></SkillsItem>
                <SkillsItem skill="React" key="react" level={4}></SkillsItem>
                <SkillsItem skill="Javascript" key="js" level={5}></SkillsItem>
                <SkillsItem skill="MongoDB" key="mongo" level={3}></SkillsItem>
                <SkillsItem skill="HTML" key="html" level={5}></SkillsItem>
                <SkillsItem skill="CSS" key="css" level={5}></SkillsItem>
              </div>
            </div>

            <div className="col-span-full flex flex-col md:col-span-1">
              <div className="flex h-12 items-center justify-center text-xl font-bold text-slate-800">
                Core Competencies and Certifications
              </div>
              <div className=" grid grow grid-cols-1 text-xl lg:grid-cols-2">
                <div className="flex flex-col items-center justify-around pl-3 lg:items-start">
                  <div>Agile Software Development</div>
                  <div>Verbal and Written Communication</div>
                  <div>Technical Documentation</div>
                  <div>Data Modeling and Design</div>
                </div>
                <div className="flex flex-col items-center justify-around pr-3 lg:items-end">
                  <div>TestOut Security Pro</div>
                  <div>TestOut Network Pro</div>
                  <div className="">Kubernetes Certified Administrator</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className=" relative col-span-12 xl:col-span-4">
          <Header text="Links"></Header>
          <div className="grid h-full grid-cols-2 gap-2 rounded-lg bg-white pt-12 shadow-md md:gap-4 2xl:grid-cols-3">
            <div className=" flex flex-col items-center justify-center">
              <div className=" flex h-16 w-16 shrink-0 grow-0 items-center justify-center rounded-lg bg-violet-500">
                <MailIcon className="h-14 w-14 text-white"></MailIcon>
              </div>
              <div className="text-lg font-bold">m.halden.a@gmail.com</div>
            </div>
            <div className=" flex flex-col items-center justify-center">
              <div className=" flex h-16 w-16 shrink-0 grow-0 items-center justify-center rounded-lg bg-violet-500">
                <PhoneIcon className="h-14 w-14 text-white"></PhoneIcon>
              </div>
              <div className="text-lg font-bold">(970)714-1774</div>
            </div>
            <div className=" flex flex-col items-center justify-center">
              <div className=" flex h-16 w-16 shrink-0 grow-0 items-center justify-center rounded-lg bg-violet-500">
                <FaLinkedin className="h-14 w-14 text-white"></FaLinkedin>
              </div>
              <div className="text-lg font-bold">/in/mhaldena</div>
            </div>
            <div className=" flex flex-col items-center justify-center">
              <div className=" flex h-16 w-16 shrink-0 grow-0 items-center justify-center rounded-lg bg-violet-500">
                <FaGithubSquare className="h-14 w-14 text-white"></FaGithubSquare>
              </div>
              <div className="text-lg font-bold">/millerbyte</div>
            </div>
            <div className=" flex flex-col items-center justify-center">
              <div className=" flex h-16 w-16 shrink-0 grow-0 items-center justify-center rounded-lg bg-violet-500">
                <FaCodepen className="h-12 w-12 text-white"></FaCodepen>
              </div>
              <div className="text-lg font-bold">codepen.io/millerbyte</div>
            </div>
          </div>
        </div>
      </div>
      {/* <Link href="/">
        <button className="fixed bottom-3 right-3 flex items-center rounded bg-gradient-to-tl from-violet-800 to-violet-400 px-4 py-2 text-lg font-bold text-white print:hidden">
          <ArrowLeftIcon className="mr-2 h-6 w-6"></ArrowLeftIcon>Home
        </button>
      </Link> */}
    </div>
  );
}
