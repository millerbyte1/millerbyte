import React, { Fragment } from "react";

const ExperiencePosition = ({ title, location, dates, className }) => {
  return (
    <div
      className={`flex h-full flex-col items-center justify-center ${className} p-4`}
    >
      <div className=" text-center text-xl font-bold text-violet-600">
        {title}
      </div>
      <div className=" text-md text-center font-bold text-gray-600">
        {location}
      </div>
      <div className=" text-center text-base italic text-gray-800">{dates}</div>
    </div>
  );
};

export default ExperiencePosition;
