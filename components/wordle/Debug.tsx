import React, { useContext } from "react";
import { WordleContext } from "../../contexts/WordleContext";

export default function Debug() {
  const { currentGuess, guessList, word, gameOver } = useContext(WordleContext);

  return (
    <div className=" text-mono flex flex-col gap-4 text-green-300">
      <div>
        <p className=" font-mono font-bold">Current Guess:</p>
        <p className=" italic">{currentGuess}</p>
      </div>
      <div>
        <p className=" font-bold">GuessList:</p>
        <div className=" italic">
          {guessList.map((g) => (
            <div key={g}>{g}</div>
          ))}
        </div>
      </div>
      <div>
        <p className=" font-bold">Word:</p>
        <p className=" italic">{word}</p>
      </div>
      <div>
        <p className=" font-bold">Game Over:</p>
        <p className=" italic">{gameOver ? "True" : "False"}</p>
      </div>
      <div>
        <p className=" font-bold">Victory:</p>
        <p className=" italic">
          {gameOver &&
          guessList[guessList.length - 1].toLowerCase() == word.toLowerCase()
            ? "True"
            : "False"}
        </p>
      </div>
    </div>
  );
}
