import React, { useContext } from "react";
import classNames from "classnames";
import { WordleContext } from "../../contexts/WordleContext";

export default function Letter({ letter }) {
  const {
    currentGuess,
    setCurrentGuess,
    AddLetterToGuess,
    gameOver,
    word,
    guessList,
  } = useContext(WordleContext);

  function OnLetterPress(e) {
    if (gameOver) return;
    const letter = e.target.innerText.toLowerCase();
    AddLetterToGuess(letter);
  }

  function LetterInGuessList() {
    for (const guess of guessList) {
      if (guess.toLowerCase().includes(letter)) {
        return true;
      }
    }

    return false;
  }

  function LetterInWord() {
    if (word.toLowerCase().includes(letter.toLowerCase())) {
      return true;
    }
    return false;
  }

  return (
    <button
      className={classNames(
        "flex h-10 w-8 items-center justify-center rounded",
        "text-md  font-bold",
        "uppercase  shadow-sm ",
        "hover:opacity-50 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2",
        LetterInGuessList()
          ? LetterInWord()
            ? "border-green-300 bg-nord-aurora-green text-nord-snow-light "
            : "border-gray-300 bg-nord-snow-dark text-nord-polar-lightest"
          : "bg-nord-polar-lightest text-nord-snow-dark",
        gameOver
          ? "bg-nord-snow-dark text-nord-polar-lightest hover:bg-nord-snow-dark hover:opacity-100"
          : ""
      )}
      onClick={(e) => OnLetterPress(e)}
      disabled={gameOver}
    >
      {letter}
    </button>
  );
}
