import React, { useContext, useEffect, useRef } from "react";
import Debug from "./Debug";
import Letter from "./LetterInput";
import Word from "./Word";
import { WordleContext, WordleProvider } from "../../contexts/WordleContext";
import classNames from "classnames";
import { BackspaceIcon } from "@heroicons/react/outline";

const letters = "qwertyuiop|asdfghjkl|zxcvbnm";

export default function Game() {
  const {
    AddLetterToGuess,
    SubmitGuess,
    currentGuess,
    RemoveLetterFromGuess,
    gameOver,
    getWotd,
    word,
    setWord,
  } = useContext(WordleContext);

  useEffect(() => {
    getWotd();
  }, []);

  useEffect(() => {
    const handleKeyPress = (evt) => {
      HandleKeyboard(evt.key);
    };
    const handle = document.addEventListener("keydown", handleKeyPress);
    return () => document.removeEventListener("keydown", handleKeyPress);
  }, [HandleKeyboard]);

  function HandleKeyboard(key) {
    if (gameOver) return;

    if (key.toLowerCase() === "enter") {
      HandleSubmit();
    } else if (key.toLowerCase() == "backspace") {
      RemoveLetterFromGuess();
    } else if (key.length === 1 && /[a-zA-Z]/.test(key)) {
      AddLetterToGuess(key);
    }
  }

  function HandleSubmit() {
    if (currentGuess.length === 5 && !gameOver) SubmitGuess();
  }

  return (
    <div className="mx-auto flex max-w-xl flex-col gap-10 bg-nord-polar-darkest py-4 shadow-md md:rounded-lg md:px-8 md:py-8">
      {/* WORD GROUPS */}
      <div className="flex flex-col items-center justify-center gap-2">
        {Array(5)
          .fill(0)
          .map((w, i) => (
            <Word key={i} row={i}></Word>
          ))}
      </div>

      {/* KEYBOARD / INPUT  */}
      <div className="grid gap-2">
        <div className="flex justify-center gap-1 sm:gap-2">
          {letters
            .split("|")[0]
            .split("")
            .map((l) => (
              <Letter letter={l} key={l}></Letter>
            ))}
        </div>
        <div className="flex justify-center gap-1 sm:gap-2">
          {letters
            .split("|")[1]
            .split("")
            .map((l) => (
              <Letter letter={l} key={l}></Letter>
            ))}
        </div>
        <div className="flex justify-center gap-1 sm:gap-2">
          {letters
            .split("|")[2]
            .split("")
            .map((l) => (
              <Letter letter={l} key={l}></Letter>
            ))}
        </div>
      </div>
      <div className="flex justify-between px-8">
        <button
          className={classNames(
            "flex items-center gap-1 rounded-xl bg-nord-frost-blue px-3 py-2 font-mono text-lg font-bold uppercase text-nord-snow-light hover:bg-nord-aurora-red focus:ring-2 focus:ring-indigo-100",
            gameOver ? "opacity-30 hover:bg-nord-frost-blue" : ""
          )}
          onClick={RemoveLetterFromGuess}
          disabled={gameOver}
        >
          <BackspaceIcon className="inline-block h-8 w-8"></BackspaceIcon>
          Del
        </button>
        <button
          className={classNames(
            "flex items-center gap-1 rounded-xl bg-nord-frost-blue px-3 py-2 font-mono text-lg font-bold uppercase text-nord-snow-light hover:bg-nord-aurora-green focus:ring-2 focus:ring-indigo-100",
            gameOver ? "opacity-30 hover:bg-nord-frost-blue" : ""
          )}
          onClick={HandleSubmit}
          disabled={gameOver}
        >
          Submit
        </button>
      </div>
    </div>
  );
}
