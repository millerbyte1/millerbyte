import React, { useContext } from "react";
import classNames from "classnames";
import { WordleContext } from "../../contexts/WordleContext";

export default function Letter({ value = "", locked = false, index }) {
  const { word } = useContext(WordleContext);

  const hasLetter = () => {
    return value !== "";
  };

  const isValidLetterPosition = () => {
    return hasLetter() && word[index].toLowerCase() == value.toLowerCase();
  };

  const isValidLetter = () => {
    return hasLetter() && word.toLowerCase().indexOf(value.toLowerCase()) > -1;
  };

  return (
    <div
      className={classNames(
        "h-10 w-10 shrink-0 rounded",
        "flex items-center justify-center",
        "text-xl font-bold uppercase",
        !locked
          ? "bg-nord-polar-lightest text-nord-snow-light"
          : isValidLetterPosition()
          ? "border-green-200 bg-nord-aurora-green text-nord-snow-light "
          : isValidLetter()
          ? "border-orange-200 bg-nord-aurora-orange text-nord-snow-light"
          : " border-gray-200 bg-gray-100 text-nord-polar-light"
      )}
    >
      {value}
    </div>
  );
}
