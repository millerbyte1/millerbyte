import React, { useContext } from "react";
import { WordleContext } from "../../contexts/WordleContext";
import Letter from "./Letter";

export default function Word({ row }) {
  const { guessList, currentGuess } = useContext(WordleContext);
  let word = "";

  if (row === guessList.length) {
    word = currentGuess;
  } else {
    word = guessList[row];
  }

  const letters = Array(5)
    .fill(0)
    .map((w, i) => {
      return (
        <Letter
          key={i}
          value={word ? word[i] : ""}
          locked={row < guessList.length}
          index={i}
        ></Letter>
      );
    });
  return <div className="mx-auto inline-flex gap-2">{letters}</div>;
}
