import React from "react";
import { useContext, useEffect } from "react";
import { TodosContext } from "../contexts/TodosContext";
import Todo from "./Todo";
import TodoForm from "./TodoForm";
import Head from "next/head";
import { useUser } from "@auth0/nextjs-auth0";
import Link from "next/link";

export default function Todos({ initialTodos }) {
  const { todos, setTodos } = useContext(TodosContext);
  const { user, error, isLoading } = useUser();

  useEffect(() => {
    setTodos(initialTodos);
  }, [todos]);

  return (
    <div className="m-auto max-w-xl p-2">
      <Head>
        <title>My Todo CRUD App</title>
      </Head>

      <main>
        <nav>
          <div className="flex items-center justify-between py-4  ">
            <div className="flex items-center justify-between">
              <div className="text-2xl font-bold text-gray-800 md:text-3xl">
                <Link href="#">My Todos</Link>
              </div>
            </div>
            <div className="flex">
              {user ? (
                <Link
                  href="/api/auth/logout"
                  className="rounded bg-blue-500 py-2 px-4 text-white hover:bg-blue-600"
                >
                  Logout
                </Link>
              ) : (
                <Link
                  href="/api/auth/login"
                  className="rounded bg-blue-500 py-2 px-4 text-white hover:bg-blue-600"
                >
                  Login
                </Link>
              )}
            </div>
          </div>
        </nav>
        {user ? (
          <>
            <TodoForm />
            <ul>
              {todos && todos.map((todo) => <Todo todo={todo} key={todo.id} />)}
            </ul>
          </>
        ) : (
          <p className="mt-4 text-center">Please login to save todos!</p>
        )}
      </main>
    </div>
  );
}
