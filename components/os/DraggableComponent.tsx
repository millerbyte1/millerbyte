import React, { useEffect, useState } from "react";
import Draggable from "react-draggable";
import { FolderOpenIcon } from "@heroicons/react/solid";
import classNames from "classnames";

export default function DraggableComponent({
  children,
  title,
  closed,
  setClosed,
}) {
  const [Fullscreen, setFullscreen] = useState(false);
  const HandleFullscreen = (toggle) => {
    setFullscreen(toggle);
  };

  return (
    <Draggable
      handle=".handle"
      position={Fullscreen ? { x: 0, y: 0 } : null}
      disabled={Fullscreen}
    >
      <div
        className={classNames(
          "flex flex-col rounded-lg border-2 border-black/10 shadow-lg",
          Fullscreen ? "h-screen w-full" : "max-h-screen max-w-fit",
          closed ? "hidden" : ""
        )}
      >
        <div className="  sticky top-0 z-10 flex h-12 flex-none  items-center justify-between space-x-6 rounded-t-lg bg-nord-polar-dark p-3">
          <FolderOpenIcon className="handle h-6 w-6 flex-none cursor-pointer text-nord-snow-dark/80"></FolderOpenIcon>
          <div className="handle flex grow cursor-pointer justify-center text-center">
            <p className="font-mono font-bold uppercase leading-snug text-nord-snow-dark/80">
              /
            </p>
            <p className="font-mono font-bold uppercase text-nord-snow-dark/80">
              {title}
            </p>
          </div>
          <div className="flex flex-none items-center space-x-4">
            <span className="inline-block h-5 w-5 cursor-pointer rounded-full bg-nord-aurora-yellow hover:bg-white md:h-3 md:w-3"></span>
            <span
              className="inline-block h-5 w-5 cursor-pointer rounded-full bg-nord-aurora-green hover:bg-white md:h-3 md:w-3"
              onClick={() => HandleFullscreen(!Fullscreen)}
            ></span>
            <span
              className="inline-block h-5 w-5 cursor-pointer rounded-full bg-nord-aurora-red hover:bg-white md:h-3 md:w-3"
              onClick={() => setClosed(true)}
            ></span>
          </div>
        </div>
        <div className=" grow items-center justify-center overflow-y-auto rounded-b-lg bg-white/20 backdrop-blur-sm md:p-4 ">
          {children}
        </div>
      </div>
    </Draggable>
  );
}
