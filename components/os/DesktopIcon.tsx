import classNames from "classnames";
import Link from "next/link";
import React from "react";
import useThemeColor from "../../hooks/useThemeColor";

export default function DesktopIcon({ title, Icon, href, color }) {
  return (
    <Link href={href}>
      <button className="group flex cursor-pointer flex-col items-center  justify-center ">
        <div className="flex h-16 w-16 items-center justify-center rounded-lg border-2 border-black/40 bg-nord-polar-lightest shadow-md">
          <Icon
            className={classNames(
              "h-14 w-14  group-hover:text-white",
              color == "red"
                ? "text-nord-aurora-red"
                : color == "orange"
                ? "text-nord-aurora-orange"
                : color == "yellow"
                ? "text-nord-aurora-yellow"
                : color == "green"
                ? "text-nord-aurora-green"
                : color == "blue"
                ? "text-nord-frost-blue"
                : color == "violet"
                ? "text-nord-aurora-violet"
                : "text-nord-snow-dark"
            )}
          ></Icon>
        </div>
        <p className="font-mono font-semibold text-nord-snow-dark group-hover:text-white">
          {title}
        </p>
      </button>
    </Link>
  );
}
