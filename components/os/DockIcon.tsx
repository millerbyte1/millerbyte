import classNames from "classnames";
import Link from "next/link";
import React from "react";

export default function DockIcon({ href, Icon, title, color }) {
  return (
    <Link href={href}>
      <button className="group flex cursor-pointer flex-col items-center  justify-center rounded-lg hover:bg-white/10">
        <Icon className="h-14 w-14 text-nord-frost-blue/50 group-hover:text-white"></Icon>
        <p
          className={classNames(
            "font-mono font-semibold  group-hover:text-white",
            color == "red"
              ? "text-nord-aurora-red"
              : color == "orange"
              ? "text-nord-aurora-orange"
              : color == "yellow"
              ? "text-nord-aurora-yellow"
              : color == "green"
              ? "text-nord-aurora-green"
              : color == "blue"
              ? "text-nord-frost-blue"
              : color == "violet"
              ? "text-nord-aurora-violet"
              : "text-nord-snow-dark"
          )}
        >
          {title}
        </p>
      </button>
    </Link>
  );
}
