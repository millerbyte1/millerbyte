import React, { useContext } from "react";
import { TodosContext } from "../contexts/TodosContext.js";

export default function Todo({ todo }) {
  const { updateTodo, deleteTodo } = useContext(TodosContext);
  const handleToggleCompleted = async () => {
    const updatedFields = {
      ...todo.fields,
      completed: !todo.fields.completed,
    };
    const updatedTodo = { id: todo.id, fields: updatedFields };
    updateTodo(updatedTodo);
  };

  return (
    <li className="my-2 flex items-center rounded-lg bg-white py-2 px-4 shadow-lg">
      <input
        name="completed"
        type="checkbox"
        checked={todo.fields.completed}
        onChange={handleToggleCompleted}
        className="form-checkbox mr-2 h-5 w-5"
      />
      <span
        className={`flex-1 text-gray-800  ${
          todo.fields.completed ? "line-through" : ""
        }`}
      >
        {todo.fields.description}
      </span>
      <button
        type="button"
        className="focus:shadow-outline rounded bg-red-500 py-1 px-2 text-sm text-white hover:bg-red-600 focus:outline-none"
        onClick={() => deleteTodo(todo.id)}
      >
        Delete
      </button>
    </li>
  );
}
