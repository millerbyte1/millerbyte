import classNames from "classnames";
import React from "react";
import Card from "../components/swift/Card";
import Icon from "../components/swift/Icon";
import {
  CalendarIcon,
  ChartBarIcon,
  FolderIcon,
  HomeIcon,
  InboxIcon,
  MenuIcon,
  UsersIcon,
  XIcon,
  UserIcon,
  ChevronRightIcon,
  ChevronDownIcon,
} from "@heroicons/react/outline";

const HandleMenuClick = (e) => {
  const grid = document.getElementById(`${e.target.id}_grid`);
  if (grid.classList.contains("hidden")) {
    grid.classList.remove("hidden");
    document.getElementById(`${e.target.id}_right`).classList.add("hidden");
    document.getElementById(`${e.target.id}_down`).classList.remove("hidden");
  } else {
    grid.classList.add("hidden");
    document.getElementById(`${e.target.id}_right`).classList.remove("hidden");
    document.getElementById(`${e.target.id}_down`).classList.add("hidden");
  }
};

export default function Swift() {
  return (
    <div className="flex h-screen w-[340px] flex-col border border-gray-200 p-2">
      <div className="flex-1 space-y-4">
        <div
          className="group mx-4 flex cursor-pointer items-center justify-between"
          onClick={HandleMenuClick}
          id="swift"
        >
          <div className="flex items-center justify-center">
            <FolderIcon className="mr-3 h-6 w-6 text-gray-500 group-hover:text-black"></FolderIcon>
            <div className=" font-semibold text-gray-500 group-hover:text-black">
              Swift
            </div>
          </div>
          <ChevronRightIcon
            className="h-4 w-4 text-gray-500 group-hover:text-black"
            id="swift_right"
          ></ChevronRightIcon>
          <ChevronDownIcon
            className="hidden h-4 w-4 text-gray-500 group-hover:text-black"
            id="swift_down"
          ></ChevronDownIcon>
        </div>
        <div className="grid hidden grid-cols-3 gap-4 p-2" id="swift_grid">
          <div className="h-24 w-24 cursor-pointer rounded bg-gray-200 hover:opacity-60 "></div>
          <div className="h-24 w-24 cursor-pointer rounded bg-gray-200 hover:opacity-60 "></div>
          <div className="h-24 w-24 cursor-pointer rounded bg-gray-200 hover:opacity-60 "></div>
          <div className="h-24 w-24 cursor-pointer rounded bg-gray-200 hover:opacity-60 "></div>
          <div className="h-24 w-24 cursor-pointer rounded bg-gray-200 hover:opacity-60 "></div>
          <div className="h-24 w-24 cursor-pointer rounded bg-gray-200 hover:opacity-60 "></div>
          <div className="h-24 w-24 cursor-pointer rounded bg-gray-200 hover:opacity-60 "></div>
        </div>
      </div>
    </div>
  );
}
