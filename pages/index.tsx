import { useEffect, useState } from "react";
import {
  CalendarIcon,
  ChartBarIcon,
  FolderIcon,
  HomeIcon,
  InboxIcon,
  MenuIcon,
  UsersIcon,
  XIcon,
  UserIcon,
} from "@heroicons/react/solid";
import { FaHSquare } from "react-icons/fa";
import { BiMovie } from "react-icons/bi";
import Link from "next/link";
import DraggableComponent from "../components/os/DraggableComponent";
import { MdOutlineVideogameAsset } from "react-icons/md";
import DesktopIcon from "../components/os/DesktopIcon";
import { RiProfileLine } from "react-icons/ri";

export default function Home({ setFullscreen }) {
  const [Closed, setClosed] = useState(false);

  return (
    <>
      {/* <div className="grid grid-cols-1 gap-4 md:grid-cols-4">
        <Link href="/resume">
          <div className="flex cursor-pointer flex-col items-center justify-center rounded-md bg-white p-4 shadow hover:opacity-50">
            <div className="flex h-24 w-24 items-center justify-center rounded-lg bg-sky-600">
              <UserIcon className="h-20 w-20 text-white"></UserIcon>
            </div>
            <h2 className="text-2xl font-semibold text-sky-600">Resume</h2>
          </div>
        </Link>

        <Link href="/wordle">
          <div className="flex cursor-pointer flex-col items-center justify-center rounded-md bg-white p-4 shadow hover:opacity-50">
            <div className="flex h-24 w-24 items-center justify-center rounded-lg bg-orange-600">
              <FaHSquare className="h-20 w-20 text-white"></FaHSquare>
            </div>
            <h2 className="text-2xl font-semibold text-orange-600">Wordle</h2>
          </div>
        </Link>

        <Link href="/movies">
          <div className="flex cursor-pointer flex-col items-center justify-center rounded-md bg-white p-4 shadow hover:opacity-50">
            <div className="flex h-24 w-24 items-center justify-center rounded-lg bg-purple-600">
              <BiMovie className="h-20 w-20 text-white"></BiMovie>
            </div>
            <h2 className="text-2xl font-semibold text-purple-600">Movies</h2>
          </div>
        </Link>
      </div> */}
      <div className="flex h-full w-full flex-wrap items-center justify-center gap-6 lg:gap-24">
        <DesktopIcon
          href="/resume"
          Icon={RiProfileLine}
          title="Resume"
          color="violet"
        ></DesktopIcon>
        <DesktopIcon
          href="/wordle"
          Icon={MdOutlineVideogameAsset}
          title="Wordle"
          color="red"
        ></DesktopIcon>
        <DesktopIcon
          href="/movies"
          Icon={BiMovie}
          title="Movies"
          color="yellow"
        ></DesktopIcon>
      </div>
    </>
  );
}
