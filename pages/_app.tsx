import { UserProvider } from "@auth0/nextjs-auth0";
import MobileSidebar from "../components/navigation/MobileSidebar";
import Sidebar from "../components/navigation/Sidebar";
import { useState } from "react";

import "../styles/globals.css";
// import MenuIcon from "@heroicons/react/outline/MenuIcon";
import PageHeader from "../components/PageHeader";
import { useRouter } from "next/router";
import classNames from "classnames";
import { MenuIcon, UserCircleIcon, HomeIcon } from "@heroicons/react/outline";
import { FolderIcon } from "@heroicons/react/solid";
import { RiProfileLine } from "react-icons/ri";
import { MdOutlineVideogameAsset } from "react-icons/md";
import { BiMove, BiMovie } from "react-icons/bi";
import Link from "next/link";
import { Disclosure, Transition } from "@headlessui/react";
import DockIcon from "../components/os/DockIcon";

function MyApp({ Component, pageProps }) {
  // You can optionally pass the `user` prop from pages that require server-side
  // rendering to prepopulate the `useUser` hook.
  const { user } = pageProps;

  const [sidebarOpen, setSidebarOpen] = useState(false);
  const [fullscreen, setFullscreen] = useState(true);
  const router = useRouter();

  return (
    <>
      <div className="absolute inset-0 -z-50 h-full w-full bg-[url('/endless-constellation.svg')]"></div>

      <UserProvider user={user}>
        <nav className="relative md:hidden">
          <Disclosure>
            <Disclosure.Button className="absolute top-4 left-4 z-20 rounded-lg border-black/80 bg-nord-polar-darkest p-3 text-nord-snow-dark shadow">
              <MenuIcon className="h-6 w-6"></MenuIcon>
            </Disclosure.Button>
            <Transition
              enter="transition duration-100 ease-out"
              enterFrom="transform scale-95 opacity-0"
              enterTo="transform scale-100 opacity-100"
              leave="transition duration-75 ease-out"
              leaveFrom="transform scale-100 opacity-100"
              leaveTo="transform scale-95 opacity-0"
            >
              <Disclosure.Panel className="absolute top-4 left-4 z-20 mt-14 flex flex-col rounded-lg border-2 border-black/10 bg-nord-polar-darkest shadow-lg">
                <div className=" text-md m-3 flex items-center justify-between font-mono font-bold uppercase text-nord-snow-dark/60">
                  <FolderIcon className="inline h-6 w-6 flex-none"></FolderIcon>
                  <div className="ml-4 flex grow justify-center text-center">
                    <p className="leading-snug">~/</p>
                    <p className=" ">Nav</p>
                  </div>
                  <div className="w-6 flex-none"></div>
                </div>
                <div className="grid grow grid-cols-4 grid-rows-1 gap-4 p-4">
                  <DockIcon
                    Icon={RiProfileLine}
                    title="Resume"
                    href="/resume"
                    color="violet"
                  ></DockIcon>
                  <DockIcon
                    Icon={MdOutlineVideogameAsset}
                    title="Wordle"
                    href="/wordle"
                    color="red"
                  ></DockIcon>
                  <DockIcon
                    Icon={BiMovie}
                    title="Movies"
                    href="/movies"
                    color="yellow"
                  ></DockIcon>
                  <DockIcon
                    Icon={HomeIcon}
                    title="Home"
                    href="/"
                    color="blue"
                  ></DockIcon>
                </div>
                <div className=" text-md bottom-0 flex h-12 flex-none items-center justify-center rounded-b-lg bg-nord-polar-dark font-mono font-bold uppercase text-nord-snow-dark/60">
                  <p className="">Login</p>
                </div>
              </Disclosure.Panel>
            </Transition>
          </Disclosure>
        </nav>
        <aside className="absolute z-10 hidden h-full flex-col justify-center p-2 md:flex">
          <nav className=" flex w-full flex-col rounded-lg border-2 border-black/10 bg-nord-polar-darkest shadow-lg">
            <div className=" text-md m-3 flex items-center justify-between font-mono font-bold uppercase text-nord-snow-dark/60">
              <FolderIcon className="inline h-6 w-6 flex-none"></FolderIcon>
              <div className="ml-4 flex grow justify-center text-center">
                <p className="leading-snug">~/</p>
                <p className=" ">Nav</p>
              </div>
              <div className="w-6 flex-none"></div>
            </div>
            <div className="grid grow grid-cols-1 grid-rows-3 gap-4 p-4">
              <DockIcon
                Icon={RiProfileLine}
                title="Resume"
                href="/resume"
                color="violet"
              ></DockIcon>
              <DockIcon
                Icon={MdOutlineVideogameAsset}
                title="Wordle"
                href="/wordle"
                color="red"
              ></DockIcon>
              <DockIcon
                Icon={BiMovie}
                title="Movies"
                href="/movies"
                color="yellow"
              ></DockIcon>
              <DockIcon
                Icon={HomeIcon}
                title="Home"
                href="/"
                color="blue"
              ></DockIcon>
            </div>
            <div className=" text-md bottom-0 flex h-12 flex-none items-center justify-center rounded-b-lg bg-nord-polar-dark font-mono font-bold uppercase text-nord-snow-dark/60">
              <p className="">Login</p>
            </div>
          </nav>
        </aside>
        {/* <Sidebar fullscreen={fullscreen}></Sidebar>
        <MobileSidebar
          sidebarOpen={sidebarOpen}
          setSidebarOpen={setSidebarOpen}
        ></MobileSidebar>
        <div
          className={classNames(
            "flex h-full flex-1 flex-col",
            !fullscreen ? "md:pl-64" : ""
          )}
        >
          <div className="sticky top-0 z-10 bg-gray-100 pl-1 pt-1 sm:pl-3 sm:pt-3 md:hidden">
            <button
              type="button"
              className="-ml-0.5 -mt-0.5 inline-flex h-12 w-12 items-center justify-center rounded-md text-gray-500 hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500"
              onClick={() => setSidebarOpen(true)}
            >
              <span className="sr-only">Open sidebar</span>
              <MenuIcon className="h-6 w-6" aria-hidden="true" />
            </button>
          </div>
          <main className="flex-1">
            <div className={classNames("h-full", !fullscreen ? "py-6" : "")}>
              <PageHeader fullscreen={fullscreen}></PageHeader>
              <div
                className={classNames(
                  !fullscreen ? "mx-auto max-w-7xl px-4 sm:px-6 md:px-8" : ""
                )}
              >
                <div className={classNames(!fullscreen ? "py-4" : "")}>
                  <Component {...pageProps} setFullscreen={setFullscreen} />
                </div>
              </div>
            </div>
          </main>
        </div> */}
        <main className=" flex h-full items-center justify-center md:ml-40">
          <Component {...pageProps} />
        </main>
      </UserProvider>
    </>
  );
}

export default MyApp;
