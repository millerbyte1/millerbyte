import React, { useEffect, useRef, useState } from "react";
import DraggableComponent from "../components/os/DraggableComponent";
import Debug from "../components/wordle/Debug";
import Game from "../components/wordle/Game";
import Letter from "../components/wordle/LetterInput";
import Word from "../components/wordle/Word";
import { WordleContext, WordleProvider } from "../contexts/WordleContext";

export default function Wordle({ setFullscreen }) {
  const [Closed, setClosed] = useState(false);

  useEffect(() => {
    setClosed(false);
  }, []);

  return (
    <DraggableComponent title="Wordle" closed={Closed} setClosed={setClosed}>
      <WordleProvider>
        {/* GAME */}
        <Game></Game>

        {/* <div className="col-span-12 rounded-lg bg-white p-8 shadow-md lg:col-span-4">
          <Debug></Debug>
        </div> */}
      </WordleProvider>
    </DraggableComponent>
  );
}
