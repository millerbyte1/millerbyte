import { useEffect, useState } from "react";
import { connectToDatabase } from "../util/mongodb";
import { SiRottentomatoes } from "react-icons/si";
import DraggableComponent from "../components/os/DraggableComponent";

export default function Movies({ movies }) {
  const [Closed, setClosed] = useState(false);

  return (
    <DraggableComponent
      title="Random Movie List"
      closed={Closed}
      setClosed={setClosed}
    >
      <div className="grid gap-4 sm:grid-cols-2 lg:grid-cols-3">
        {movies.map((movie) => {
          return (
            <div
              className="flex flex-col items-center justify-between rounded-xl bg-white shadow"
              key={movie._id}
            >
              {movie.poster && (
                <img
                  src={movie.poster}
                  className="mb-4 w-full grow-0 rounded-t-xl"
                ></img>
              )}
              <div className="flex items-center">
                <SiRottentomatoes className=" mr-4 h-12 w-12 text-red-500"></SiRottentomatoes>
                <span className=" text-2xl font-semibold text-gray-700">
                  {movie.tomatoes?.viewer.meter}%
                </span>
              </div>
              <h2 className="mx-2 shrink grow-0 text-2xl font-semibold text-gray-700">
                {movie.title}
              </h2>
              <p className=" mx-8 mb-8 text-sm font-light text-gray-500">
                {movie.plot}
              </p>
            </div>
          );
        })}
      </div>
    </DraggableComponent>
  );
}

export async function getServerSideProps() {
  const { db } = await connectToDatabase("sample_mflix");

  const movies = await db
    .collection("movies")
    .aggregate([{ $sample: { size: 20 } }])
    .toArray();

  // const movies = await db
  //   .collection("movies")
  //   .find({})
  //   .sort({ metacritic: -1 })
  //   .limit(20)
  //   .toArray();

  return {
    props: {
      movies: JSON.parse(JSON.stringify(movies)),
    },
  };
}
