import { getSession } from "@auth0/nextjs-auth0";
import { table, minifyRecords } from "./api/utils/airtable";
import React, { useContext, useEffect, useState } from "react";
import { FieldSet, Records } from "airtable";
import TodoForm from "../components/TodoForm";
import Todo from "../components/Todo";
import TodoContainer from "../components/Todos";
import { TodosProvider } from "../contexts/TodosContext";

export default function todos({ initialTodos, user }) {
  return (
    <TodosProvider>
      <TodoContainer initialTodos={initialTodos}></TodoContainer>
    </TodosProvider>
  );
}

export async function getServerSideProps(context) {
  const session = await getSession(context.req, context.res);
  let todoData = [];
  if (session?.user) {
    todoData = await table
      .select({ filterByFormula: `userId = '${session.user.sub}'` })
      .firstPage();
  }
  return {
    props: {
      initialTodos: minifyRecords(todoData),
      user: session?.user || null,
    },
  };
}
