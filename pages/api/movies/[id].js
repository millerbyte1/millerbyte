import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../util/mongodb";

export default async (req, res) => {
  const {
    method,
    query: { id },
  } = req;

  const { db } = await connectToDatabase("sample_mflix");

  switch (method) {
    case "GET":
      const movies = await db
        .collection("movies")
        .find({ _id: ObjectId(id) })
        .sort({ metacritic: -1 })
        .limit(20)
        .toArray();

      res.json(movies);
  }
};
