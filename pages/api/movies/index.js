import { connectToDatabase } from "../../../util/mongodb";

export default async (req, res) => {
  const { method } = req;

  const { db } = await connectToDatabase("sample_mflix");

  switch (method) {
    case "GET":
      const movies = await db
        .collection("movies")
        .find({})
        .sort({ metacritic: -1 })
        .limit(20)
        .toArray();

      res.json(movies);

      // Get data from your database

      break;
    case "PUT":
      // Update or create data in your database
      res.status(200).json({ id, name: name || `User ${id}` });
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
};
