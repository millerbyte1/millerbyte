import { getMinifiedRecord, minifyRecords, base } from "../utils/airtable.js";
import { withApiAuthRequired, getSession } from "@auth0/nextjs-auth0";

export default async (req, res) => {
  const table = base("wordle_words");
  try {
    const records = await table.select({ fields: ["Word"] }).firstPage();
    const formattedRecords = records.map((record) => {
      return { id: record.id, word: record.fields.Word };
    });
    res.statusCode = 200;
    res.json(
      formattedRecords[Math.floor(Math.random() * formattedRecords.length)]
    );
  } catch (error) {
    console.error(error);
    res.statusCode = 500;
    res.json({ msg: "Something went wrong" });
  }
};
