import { table, getMinifiedRecord, minifyRecords } from "./utils/airtable.js";
import { withApiAuthRequired, getSession } from "@auth0/nextjs-auth0";

export default withApiAuthRequired(async (req, res) => {
  const { user } = await auth0.getSession(req);
  try {
    const records = await table
      .select({ filterByFormula: `userId = '${user.sub}'` })
      .firstPage();
    const formattedRecords = minifyRecords(records);
    res.statusCode = 200;
    res.json(formattedRecords);
  } catch (error) {
    console.error(error);
    res.statusCode = 500;
    res.json({ msg: "Something went wrong" });
  }
});
