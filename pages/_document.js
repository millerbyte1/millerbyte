import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html className="h-full bg-gray-100">
      <Head />
      <body className="h-full">
        <Main />
        <NextScript />
        <style jsx global>{`
          /* Other global styles such as 'html, body' etc... */

          #__next {
            height: 100%;
          }
        `}</style>
      </body>
    </Html>
  );
}
