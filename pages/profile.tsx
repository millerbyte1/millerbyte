import Link from "next/link";
import React, { useEffect } from "react";
import { useUser } from "@auth0/nextjs-auth0";

export default function Profile() {
  const { user, error, isLoading } = useUser();

  return (
    <div className="space-y-4 sm:flex sm:justify-between">
      <div className="flex items-center justify-start gap-2">
        <img className="h-16 w-16 rounded-full" src={user?.picture}></img>
        <div>
          <div className=" text-lg font-medium text-gray-900">Millerbyte</div>
          <div className="text-md font-light text-gray-600">
            More info about Millerbyte
          </div>
        </div>
      </div>
      <div className="flex flex-col items-stretch sm:flex-row sm:items-center">
        <Link href="/api/auth/logout">
          <button
            type="button"
            className="inline-flex items-center justify-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 focus:ring-offset-gray-100"
          >
            Logout
          </button>
        </Link>
      </div>
    </div>
  );
}
