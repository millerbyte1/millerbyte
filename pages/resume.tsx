import React, { useEffect, useState } from "react";
import DraggableComponent from "../components/os/DraggableComponent";
import ResumeComponent from "../components/resume/Resume";

export default function Resume() {
  const [Closed, setClosed] = useState(false);

  return (
    <>
      <DraggableComponent title="Resume" closed={Closed} setClosed={setClosed}>
        <ResumeComponent></ResumeComponent>
      </DraggableComponent>
    </>
  );
}
