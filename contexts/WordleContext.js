import { createContext, useState } from "react";

const WordleContext = createContext();

const WordleProvider = ({ children }) => {
  const [word, setWord] = useState("toast");
  const [currentRow, setCurrentRow] = useState(0);
  const [currentGuess, setCurrentGuess] = useState("");
  const [guessList, setGuessList] = useState([]);
  const [gameOver, setGameOver] = useState(false);

  const getWotd = async () => {
    try {
      const res = await fetch("/api/wordle/getWotd");
      const wotd = await res.json();
      setWord(wotd.word);
    } catch (err) {
      console.error(err);
    }
  };

  const AddLetterToGuess = (letter) => {
    setCurrentGuess((currentGuess) => {
      if (currentGuess.length < 5) {
        return currentGuess + letter;
      }
      return currentGuess;
    });
  };

  const RemoveLetterFromGuess = () => {
    setCurrentGuess((currentGuess) => {
      if (currentGuess.length > 0) {
        return currentGuess.substring(0, currentGuess.length - 1);
      }
      return currentGuess;
    });
  };

  const SubmitGuess = () => {
    setGuessList((list) => {
      if (list.length == 4) setGameOver(true);
      return [...list, currentGuess];
    });
    if (currentGuess.toLowerCase() == word.toLowerCase()) {
      setGameOver(true);
    }
    setCurrentGuess("");
  };

  return (
    <WordleContext.Provider
      value={{
        word,
        setWord,
        currentRow,
        setCurrentRow,
        currentGuess,
        setCurrentGuess,
        SubmitGuess,
        AddLetterToGuess,
        guessList,
        RemoveLetterFromGuess,
        gameOver,
        setGameOver,
        getWotd,
      }}
    >
      {children}
    </WordleContext.Provider>
  );
};

export { WordleProvider, WordleContext };
